# DCS Miz Radio Changer

## Introduction

This is a simple program to edit radio presets in DCS Mission files.

Built using WPF on Dotnet Core 3.0 preview 5 x86. May require VS 2019 preview to build at this time
