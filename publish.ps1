param(
    [Parameter(Mandatory=$true)]
    [string] $version
)

dotnet publish --runtime win-x86 --self-contained -o .\bin\v$version\DcsMizRadioChanger -c Release /p:AssemblyVersion=$version /p:PublishSingleFile=true