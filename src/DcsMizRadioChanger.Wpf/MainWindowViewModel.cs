﻿using Caliburn.Micro;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using DcsMizRadioChanger.Wpf.Models;

namespace DcsMizRadioChanger.Wpf
{
    public class MainWindowViewModel : Screen
    {
        private List<string> _blueUnitTypes = new List<string>();
        private List<string> _redUnitTypes = new List<string>();
        private string _selectedType;
        private ObservableCollection<ChannelPreset> _selectedRadios0;
        private ObservableCollection<ChannelPreset> _selectedRadios1;
        private ObservableCollection<ChannelPreset> _selectedRadios2;
        private List<Unit> _blueUnits;
        private List<Unit> _redUnits;
        private MizManipulator _mizManipulator;
        private Side _selectedSide;
        private List<Side> _sides = new List<Side>();
        public string WindowTitle => "DCS Miz Radio Changer v" + GetRunningVersion();

        public List<Unit> Units
        {
            get
            {
                if (_selectedSide == Side.Red)
                    return _redUnits;

                return _blueUnits;
            }
            set
            {
                if (_selectedSide == Side.Red)
                    _redUnits = value;

                _blueUnits = value;
            }
        }
        public List<string> UnitTypes
        {
            get
            {
                if (_selectedSide == Side.Red)
                    return _redUnitTypes;

                return _blueUnitTypes;
            }
            set
            {
                if (_selectedSide == Side.Red)
                    Set(ref _redUnitTypes, value, nameof(SideSelected));

                Set(ref _blueUnitTypes, value, nameof(SideSelected));
            }
        }

        public List<Side> Sides
        {
            get => _sides;
            set => Set(ref _sides, value, nameof(DoneLoading));
        }


        public bool DoneLoading => _sides.Count > 0;
        public bool SideSelected
        {
            get
            {
                if (_selectedSide == Side.Red)
                    return _redUnitTypes.Count > 0;

                return _blueUnitTypes.Count > 0;
            }
        }

        public ObservableCollection<ChannelPreset> SelectedRadios0
        {
            get => _selectedRadios0;
            set => Set(ref _selectedRadios0, value);
        }

        public ObservableCollection<ChannelPreset> SelectedRadios1
        {
            get => _selectedRadios1;
            set => Set(ref _selectedRadios1, value);
        }

        public ObservableCollection<ChannelPreset> SelectedRadios2
        {
            get => _selectedRadios2;
            set => Set(ref _selectedRadios2, value);
        }

        public string SelectedType
        {
            get => _selectedType;
            set => Set(ref _selectedType, value);
        }

        public Side SelectedSide
        {
            get => _selectedSide;
            set => Set(ref _selectedSide, value, nameof(SideSelected));
        }

        public MainWindowViewModel()
        {
        }

        public void LoadMiz()
        {
            var fileDialog = new OpenFileDialog();
            fileDialog.Filter = "Miz file (*.miz)|*.miz|All files (*.*)|*.*";

            if (fileDialog.ShowDialog() == false)
                return;

            var mizPath = fileDialog.FileName;

            _mizManipulator = new MizManipulator();

            var coalitions = _mizManipulator.ReadMiz(mizPath);

            _blueUnits = new List<Unit>();
            _redUnits = new List<Unit>();

            UnitTypes = new List<string>();

            if (AddUnit(coalitions.Blue.Units, Side.Blue))
                Sides.Add(Side.Blue);

            if (AddUnit(coalitions.Red.Units, Side.Red))
                Sides.Add(Side.Red);

            UnitTypes.Sort();
            SelectedSide = Sides.FirstOrDefault();
            NotifyOfPropertyChange(nameof(Sides));
            NotifyOfPropertyChange(nameof(DoneLoading));
            NotifyOfPropertyChange(nameof(SelectedSide));
            //MessageBox.Show("Done loading miz", "Done", MessageBoxButton.OK);
        }

        private Version GetRunningVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version;
        }

        private bool AddUnit(IEnumerable<Unit> units, Side side)
        {
            var addedUnits = 0;
            foreach (var unit in units)
            {
                //if (!unit.skill.Equals("Client", StringComparison.OrdinalIgnoreCase))
                //    continue;

                if (unit.Radio == null || !unit.Radio.Any())
                    continue;


                switch (side)
                {
                    case Side.Blue:
                        if (_blueUnitTypes.Any(ut => ut == unit.type))
                            continue;

                        _blueUnits.Add(unit);
                        _blueUnitTypes.Add(unit.type);
                        addedUnits++;
                        break;
                    case Side.Red:
                        if (_redUnitTypes.Any(ut => ut == unit.type))
                            continue;

                        _redUnits.Add(unit);
                        _redUnitTypes.Add(unit.type);
                        addedUnits++;
                        break;
                    default:
                        break;
                }
            }
            return addedUnits > 0;

        }

        public bool CanSaveChannels => !string.IsNullOrEmpty(SelectedType);

        public void SaveChannels()
        {
            var units = Units.Where(u => u.type == SelectedType);

            foreach (var unit in units)
            {
                var numRadios = unit.Radio.Length;
                if (numRadios >= 1)
                {
                    for (var i = 0; i < unit.Radio[0].channels.Length; i++)
                    {
                        unit.Radio[0].channels[i] = SelectedRadios0[i].Freq;
                    }
                }

                if (numRadios >= 2)
                {
                    for (var i = 0; i < unit.Radio[1].channels.Length; i++)
                    {
                        unit.Radio[1].channels[i] = SelectedRadios1[i].Freq;
                    }
                }

                if (numRadios >= 3)
                {
                    for (var i = 0; i < unit.Radio[2].channels.Length; i++)
                    {
                        unit.Radio[2].channels[i] = SelectedRadios2[i].Freq;
                    }
                }
            }
        }

        public bool CanSaveMiz => !string.IsNullOrEmpty(SelectedType);

        public void SaveMiz()
        {
            if (_mizManipulator.SaveMiz(_blueUnits, _redUnits))
                MessageBox.Show("Done saving miz", "Done", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public void OnSideChangedAction()
        {
            SelectedType = UnitTypes.FirstOrDefault();
            NotifyOfPropertyChange(nameof(UnitTypes));
            NotifyOfPropertyChange(nameof(SideSelected));
            NotifyOfPropertyChange(nameof(SelectedType));
        }

        public void OnSelectionChangedAction()
        {
            SelectedRadios0 = null;
            SelectedRadios1 = null;
            SelectedRadios2 = null;
            if (!string.IsNullOrEmpty(SelectedType))
            {
                var unit = Units.FirstOrDefault(u => u.type == SelectedType);
                if (unit == null)
                    return;

                var numRadios = unit.Radio.Length;
                if (numRadios >= 1)
                {
                    var collection = new ObservableCollection<ChannelPreset>();
                    var count = 1;
                    foreach (var channel in unit.Radio[0].channels)
                    {
                        collection.Add(new ChannelPreset()
                        {
                            Id = count++,
                            Freq = channel
                        });
                    }

                    SelectedRadios0 = collection;
                }

                if (numRadios >= 2)
                {
                    var collection = new ObservableCollection<ChannelPreset>();
                    var count = 1;
                    foreach (var channel in unit.Radio[1].channels)
                    {
                        collection.Add(new ChannelPreset()
                        {
                            Id = count++,
                            Freq = channel
                        });
                    }

                    SelectedRadios1 = collection;
                }

                if (numRadios >= 3)
                {
                    var collection = new ObservableCollection<ChannelPreset>();
                    var count = 1;
                    foreach (var channel in unit.Radio[2].channels)
                    {
                        collection.Add(new ChannelPreset()
                        {
                            Id = count++,
                            Freq = channel
                        });
                    }

                    SelectedRadios2 = collection;
                }

                NotifyOfPropertyChange(nameof(CanSaveChannels));
                NotifyOfPropertyChange(nameof(CanSaveMiz));

                Console.WriteLine(unit);
            }
        }
    }
}
