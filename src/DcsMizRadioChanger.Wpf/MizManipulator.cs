﻿using DcsMizRadioChanger.Wpf.Models;
using LuaInterface;
using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Text.RegularExpressions;

namespace DcsMizRadioChanger.Wpf
{
    class MizManipulator
    {
        private Lua _lua;
        private string _originalMizPath;

        public Coalitions ReadMiz(string mizPath)
        {
            _originalMizPath = mizPath;
            string mizData;

            using (ZipArchive miz = ZipFile.OpenRead(mizPath))
            {
                var mission = miz.GetEntry("mission");
                if (mission == null)
                    return null;

                using var sr = new StreamReader(mission.Open(), Encoding.UTF8);
                mizData = sr.ReadToEnd();
            }

            mizData = Regex.Replace(mizData, @"\r\n?|\n", Environment.NewLine);

            return ConvertLuaToCs(mizData);
        }

        private Coalitions ConvertLuaToCs(string luaTable)
        {
            _lua = new Lua();

            var objects = _lua.DoString(luaTable);

            _lua.DoString(@"dofile('injectRadio.lua')");

            _lua.DoString(@"blueUnits = extractRadio('blue')");
            _lua.DoString(@"redUnits = extractRadio('red')");

            var coalitions = new Coalitions();

            var blueUnits = _lua["blueUnits"] as LuaTable;
            var redUnits = _lua["redUnits"] as LuaTable;

            coalitions.Blue = CreateUnitList(blueUnits, Side.Blue);
            coalitions.Red = CreateUnitList(redUnits, Side.Red);

            
            return coalitions;
        }

        private static UnitList CreateUnitList(LuaTable blueUnits, Side side)
        {
            var unitList = new UnitList(side);

            if (blueUnits == null)
                return unitList;

            var units = new List<Unit>();
            foreach (var unitTable in blueUnits)
            {
                if (!(unitTable is DictionaryEntry luaUnit)) continue;
                if (luaUnit.Value is LuaTable unit)
                {
                    if (!(unit["Radio"] is LuaTable luaRadios)) continue;

                    var numRadios = luaRadios.Values.Count;
                    var radios = new List<Radio>(numRadios);
                    foreach (var luaRadio in luaRadios)
                    {
                        if (!(luaRadio is DictionaryEntry radio)) continue;
                        if (radio.Value is LuaTable rad)

                        {
                            var radValues = rad["channels"] as LuaTable;
                            if (radValues == null) continue;

                            var channels = new List<double>();
                            foreach (var radValue in radValues.Values)
                            {
                                channels.Add((double) radValue);
                            }

                            radios.Add(new Radio
                            {
                                channels = channels.ToArray()
                            });
                        }
                    }

                    var item = new Unit()
                    {
                        skill = unit["skill"] as string,
                        type = unit["type"] as string,
                        Radio = radios.ToArray()
                    };
                    units.Add(item);
                }
            }

            unitList.Units = units;
            return unitList;
        }

        public bool SaveMiz(List<Unit> blueUnits, List<Unit> redUnits)
        {
            var fileDialog = new SaveFileDialog();
            fileDialog.Filter = "Miz file (*.miz)|*.miz";
            if (fileDialog.ShowDialog() == false)
                return false;


            _lua.DoString(@"serializer = require('Serializer')");

            _lua.DoString(@"dofile('injectRadio.lua')");

            foreach (var unit in blueUnits)
            {
                _lua.DoString($"radio = {unit.RadioToLuaTable()}");
                _lua.DoString($"injectRadio('{unit.type}', radio, 'blue')");
            }

            foreach (var unit in redUnits)
            {
                _lua.DoString($"radio = {unit.RadioToLuaTable()}");
                _lua.DoString($"injectRadio('{unit.type}', radio, 'red')");
            }

            var serializerScript = new StringBuilder();

            var fileName = fileDialog.FileName.Replace(@"\\", @"/");

            serializerScript.AppendLine(@"local f = io.open('mission', 'w')");
            serializerScript.AppendLine(@"if f then");
            serializerScript.AppendLine(@"local s = serializer.new(f)");
            serializerScript.AppendLine(@"missionTxt = s:serialize_simple2('mission', mission)");
            serializerScript.AppendLine(@"f:close()");
            serializerScript.AppendLine(@"end");

            _lua.DoString(serializerScript.ToString());

            ZipMiz(fileDialog.FileName);

            return true;
        }

        private void ZipMiz(string fileName)
        {
            try
            {
                if (!fileName.Equals(_originalMizPath))
                {
                    File.Copy(_originalMizPath, fileName);
                }

                using var mizFile = ZipFile.Open(fileName, ZipArchiveMode.Update);

                mizFile.GetEntry("mission")?.Delete();
                ;

                mizFile.CreateEntryFromFile("mission", "mission");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
