﻿namespace DcsMizRadioChanger.Wpf.Models
{
    public class ChannelPreset
    {
        public int Id { get; set; }
        public double Freq { get; set; }
    }
}
