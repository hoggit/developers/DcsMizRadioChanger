﻿using System.Text;

namespace DcsMizRadioChanger.Wpf.Models
{
    public class Radio
    {
        public double[] channels { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine("[\"channels\"] =");
            sb.AppendLine("{");

            for (int i = 0; i < channels.Length; i++)
            {
                sb.AppendLine($"[{i + 1}] = {channels[i]},");
            }

            sb.AppendLine("},");


            return sb.ToString();
        }
    }
}
