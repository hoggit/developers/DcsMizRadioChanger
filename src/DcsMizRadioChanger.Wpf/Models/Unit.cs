﻿using System.Diagnostics;
using System.Text;

namespace DcsMizRadioChanger.Wpf.Models
{
    [DebuggerDisplay("Type = {type}")]
    public class Unit
    {
        public string skill { get; set; }
        public string type { get; set; }
        public Radio[] Radio { get; set; }


        public string RadioToLuaTable()
        {
            var sb = new StringBuilder();
            sb.AppendLine("{");

            for (int i = 0; i < Radio.Length; i++)
            {
                sb.AppendLine($"[{i + 1}] = ");
                sb.AppendLine(@"{");
                sb.AppendLine($"{Radio[i]}");
                sb.AppendLine(@"},");
            }

            sb.AppendLine("}");

            return sb.ToString();
        }
    }
}
