﻿using System.Collections.Generic;

namespace DcsMizRadioChanger.Wpf.Models
{
    public class UnitList
    {
        public List<Unit> Units { get; set; }
        public Side Side { get; }

        public UnitList(Side side)
        {
            Side = side;
        }
    }
}
