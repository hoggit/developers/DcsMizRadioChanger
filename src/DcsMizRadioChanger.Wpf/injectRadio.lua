function injectRadio(unitType, freqTable, side)
    local coalitions = mission["coalition"]
    local coalition = coalitions[side]
        local countries = coalition["country"]
        for _,country in pairs(countries) do
            local helicopters = country["helicopter"]
            local planes = country["plane"]
            injectRadioToVehicle(unitType, helicopters, freqTable)
            injectRadioToVehicle(unitType, planes, freqTable)
        end
end


function injectRadioToVehicle(unitType, vehicleTable, freqTable)
    if vehicleTable == nil then return end
    local groups = vehicleTable["group"]
    if groups == nil then return end
    for _,group in pairs(groups) do
        local units = group["units"]
        local unitTypeCorrect = false
        for _,unit in pairs(units) do
            if unit["type"] ~= unitType then
                unitTypeCorrect = false
                break
            end
            if unit["skill"] == "Client" then
                unit["Radio"] = freqTable
                unitTypeCorrect = true
            end
        end
        if unitTypeCorrect == true then
            group["frequency"] = freqTable[1]["channels"][1]
        end
    end
end


function extractRadio(side)
    local units = {}

    local coalitions = mission["coalition"]
    local coalition = coalitions[side]
    local countries = coalition["country"]
    for _,country in pairs(countries) do
        local helicopters = country["helicopter"]
        local planes = country["plane"]
        if helicopters ~= nil then
            for _,unit in pairs(extractRadioFromVehicle(helicopters)) do
                table.insert(units, unit)
            end
        end
        if planes ~= nil then
            for _,unit in pairs(extractRadioFromVehicle(planes)) do
                table.insert(units, unit)
            end
        end
    end

    return units
end


function extractRadioFromVehicle(vehicleTable)
    local unitsToReturn = {}
    if vehicleTable == nil then return end
    local groups = vehicleTable["group"]
    if groups == nil then return end
    for _,group in pairs(groups) do
        local units = group["units"]
        for _,unit in pairs(units) do
            if unit["skill"] == "Client" then
                table.insert(unitsToReturn, unit)
            end
        end
    end
    return unitsToReturn

end
